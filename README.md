# Animal Classifier

<img src="illustration.png" width="200"/>



Ce projet est un classificateur d'images spécialisé dans la distinction entre les représentations visuelles de chats et de chiens. En fonction de ce que vous donner comme image ce modèle vous retoutne si c'est est un chat ou un chien qui est présent sur l'image.
<br>
Ce projet est fondé sur un réseau neuronal convolutionnel (CNN) et a été développé à l'aide des bibliothèque Python TensorFlow et Keras. 

Les CNN, dans ce contexte, agissent comme des architectes visuels, capables d'analyser les caractéristiques des images pour prendre des décisions précises afin de détecter si c'est un chat ou un chien qui est présent sur l'image.

## Installation

1. **Cloner le projet :**
git clone https://gitlab.com/aavaury/ia_animals.git


2. **Prérequis :**

Pour ma part j'ai du créer un environnement python pour que le projet fonctionne

Création de l'environnement python
```
python -m venv mon_environnement
```

Utilisation de l'environnement python

```
mon_environnement\Scripts\activate
```

Voici les dépendances a installer pour faire fonctionner le projet

```
tensorflow==2.6.0
numpy==1.21.2
pandas==1.3.3
```

## Utilisation

1. **Entraîner le modèle :**
- Dézippez le dataset.
- Exécutez le script `ia.py` pour entraîner le modèle.

2. **Prédire un animal :**
- Exécutez le script `ia_animals.py` en fournissant le chemin d'une image à tester.
python ia_animals.py --image_path "path/to/your/image.jpg"


## Structure du projet

- `dataset/` : Contient les images de chats et de chiens pour l'entraînement.
- `ia.py` : Script pour entraîner le modèle.
- `ia_animal.py` : Script pour prédire si une image est un chat ou un chien.
- `animal_classifier_model.h5` : Fichier du modèle sauvegardé.

## Notes

- Les images doivent être au format JPG.
- Le modèle est actuellement entraîné sur un petit ensemble de données et peut nécessiter un entraînement plus long pour de meilleures performances.

---

*Ce projet a été créé par [Adrien V](https://gitlab.com/aavaury).*