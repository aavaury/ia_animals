import os
import shutil

def trier_images(source_folder, destination_folder, prefixes):
    # Assurez-vous que le dossier de destination existe
    if not os.path.exists(destination_folder):
        os.makedirs(destination_folder)

    # Parcourez tous les fichiers du dossier source
    for filename in os.listdir(source_folder):
        # Vérifiez si le nom du fichier commence par l'un des préfixes
        for prefix in prefixes:
            if filename.lower().startswith(prefix.lower()):
                # Construisez le chemin source et le chemin de destination
                source_path = os.path.join(source_folder, filename)
                destination_path = os.path.join(destination_folder, filename)

                # Déplacez le fichier vers le dossier de destination
                shutil.move(source_path, destination_path)
                print(f"Fichier {filename} déplacé avec succès.")

# Spécifiez le dossier source, le dossier de destination et les préfixes
source_folder = "dataset"
destination_folder = "cat"
prefixes = ["Siamese", "Ragdoll", "Maine_Coon", "Birman", "Abyssinian", "Bengal", "Bombay", "British_Shorthair", "Sphynx", "Egyptian_Mau", "Russian_Blue", "Persian"]

# Appelez la fonction pour trier les images
trier_images(source_folder, destination_folder, prefixes)
