from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import numpy as np

def predict_animal(model, image_path):
    # Charger et prétraiter l'image
    img = load_img(image_path, target_size=(64, 64))
    img_array = img_to_array(img)
    img_array = img_array.reshape((1, 64, 64, 3))
    img_array = img_array / 255.0

    # Faire la prédiction
    prediction = model.predict(img_array)

    # Afficher le résultat
    if prediction[0][0] < 0.5:
        return "Chat"
    else:
        return "Chien"

# Charger le modèle sauvegardé
model = load_model('animal_classifier_model.h5')

# Utiliser la fonction predict_animal avec le chemin de l'image que tu veux tester
image_path_to_test = "dataset_test/baika.jpg"
result = predict_animal(model, image_path_to_test)

print(f"La prédiction est : {result}")
