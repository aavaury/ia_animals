import os
import pandas as pd
from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from tensorflow.keras.models import Sequential, save_model, load_model
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense

#https://www.robots.ox.ac.uk/~vgg/data/pets/

def predict_animal(image_path):
    # Charger et prétraiter l'image
    img = load_img(image_path, target_size=(64, 64))
    img_array = img_to_array(img)
    img_array = img_array.reshape((1, 64, 64, 3))
    img_array = img_array / 255.0

    # Faire la prédiction
    prediction = model.predict(img_array)

    # Afficher le résultat
    if prediction[0][0] < 0.5:
        print("C'est un chat!")
    else:
        print("C'est un chien!")

# Définir les chemins des dossiers contenant les images
chemin_chats = "dataset/cat"
chemin_chiens = "dataset/dog"

# Charger les chemins des images et les étiqueter comme chats (classe 0) ou chiens (classe 1)
chemin_images_chats = [os.path.join(chemin_chats, img) for img in os.listdir(chemin_chats)]
chemin_images_chiens = [os.path.join(chemin_chiens, img) for img in os.listdir(chemin_chiens)]

df_chats = pd.DataFrame({'chemin': chemin_images_chats, 'classe': '0'})  # Convertir en chaîne
df_chiens = pd.DataFrame({'chemin': chemin_images_chiens, 'classe': '1'})  # Convertir en chaîne

df = pd.concat([df_chats, df_chiens], ignore_index=True)

# Mélanger le DataFrame
df = df.sample(frac=1).reset_index(drop=True)

# Créer des générateurs d'images avec augmentation des données
datagen_train = ImageDataGenerator(
    rescale=1./255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True
)

# Utiliser flow_from_dataframe pour charger les données
generator_train = datagen_train.flow_from_dataframe(
    df,
    x_col='chemin',
    y_col='classe',
    target_size=(64, 64),
    batch_size=32,
    class_mode='binary',
    color_mode='rgb'
)

# Créer le modèle CNN
model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(64, 64, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(units=128, activation='relu'))
model.add(Dense(units=1, activation='sigmoid'))

# Compiler le modèle
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Entraîner le modèle
model.fit(
    generator_train,
    steps_per_epoch=len(generator_train),
    epochs=80
)

# Sauvegarder le modèle
save_model(model, 'animal_classifier_model.h5')

# Utiliser la fonction predict_animal avec le chemin de l'image que tu veux tester
image_path_to_test = "test_dog.jpg"
predict_animal(image_path_to_test)
